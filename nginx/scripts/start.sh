#!/bin/bash

# Set custom webroot
if [ ! -z "$WEBROOT" ]; then
    sed -i "s#root /var/www/html;#root ${WEBROOT};#g" /etc/nginx/sites-available/default
fi

/usr/bin/supervisord -n -c /etc/supervisor/supervisord.conf